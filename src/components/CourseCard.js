	
	import { Card} from 'react-bootstrap'
	import {Link} from 'react-router-dom'
	export default function Course({courseProp}){

		console.log(courseProp)
  
  //Syntax: const {properties} = propname
  //object destructuring
  const {name, description, price, _id} = courseProp

  //array destructuring
  // const[count, setCount] = useState(0)

  
  //Syntax: const[getter, setter] = useState(initialValue)

  // Hook used is useState -  to store the state
  // const[seats, setSeats] = useState(30)

  // function enroll(){
  // 	 if(seats > 0){
  // 			setCount(count + 1);
  // 		 console.log('Enrollees' + count);
  // 		 setSeats(seats - 1);
  // 		 console.log('Seats' + seats);

  // }else{
  	
  // 			alert('No more seats available')
  // }
 	
  
  // 	}



  	 
	
    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className = "btn btn-primary" to = {`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
