//for mock database

const coursesData = [
	{
		id:"wdc001",
		name: "PHP - Lavarel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Laborum nemo neque itaque maiores culpa optio totam consectetur minima facere recusandae debitis natus consequuntur eum molestias non, reprehenderit architecto amet obcaecati.",
		price: 45000,
		onOffer: true
	},
	{
		id:"wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Laborum nemo neque itaque maiores culpa optio totam consectetur minima facere recusandae debitis natus consequuntur eum molestias non, reprehenderit architecto amet obcaecati.",
		price: 55000,
		onOffer: true
	},
	{
		id:"wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Laborum nemo neque itaque maiores culpa optio totam consectetur minima facere recusandae debitis natus consequuntur eum molestias non, reprehenderit architecto amet obcaecati.",
		price: 60000,
		onOffer: true
	}
]

export default coursesData;