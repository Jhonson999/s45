import{useState, useEffect, useContext} from 'react'
import{Form, Button} from 'react-bootstrap'
import{Redirect} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Login (){

//Allow us to consume the User Context object and it's properties to use for user validation
const{user, setUser} = useContext(UserContext)
//State hooks to store the values of the input fields
const[password, setPassword] = useState('')
	const[email, setEmail] = useState('')

	//State to determine whether registered button is enabled or not
	const[isActive, setisActive] = useState(false)

	console.log(email)
	console.log(password)
	

  function LoginUser(e){
     //Prevents page redirection via form submission
     e.preventDefault();

     fetch('http://localhost:4000/users/login',{
      method: 'POST',
      headers:{
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
     })
     .then(res => res.json())
     .then(data => {
        console.log(data)

        if(typeof data.access !== 'undefined'){
          localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: 'Login Successful!',
            icon: 'success',
            text: 'Welcome to Zuitt'
          })
        } else {
          Swal.fire({
            title: 'Authentication Failed.',
            icon: 'error',
            text: 'Check your login details'
          })
        }
     })
     //Set the email of the user in the local storage
     //Syntax:
     //localStorage.setItem('propertyName', value)
     // localStorage.setItem('email', email);

     // setUser({
     //    email:localStorage.getItem('email')
     // })

     //Clears the input fields
     setEmail('');
     setPassword('');
     

     // alert('Thank you for loggin in!')
  }

  const retrieveUserDetails = (token) => {
     fetch('http://localhost:4000/users/details', {
      headers:{
        Authorization: `Bearer ${token}`
      }
     })
     .then(res => res.json())
     .then(data => {
      console.log(data)

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
     })
  }

	useEffect(() => {
         if((email !== '' && password !== '')){
         	

         	setisActive(true)
         } else {

         	setisActive(false)
         }

	}, [email, password])


	return(

        (user.id !== null)?
            <Redirect to="/courses"/>
            :
           <Form onSubmit={(e)=>LoginUser(e)}>
               <Form.Group>
                 <Form.Label>Email Address:
                 </Form.Label>
                 <Form.Control
                   type = 'email'
                   placeholder = 'Please enter your registered email here'
                   value = {email}
                   onChange = {e => setEmail(e.target.value)}
                   required
                 />
                 <Form.Text className = "text-muted">
                 We'll never share your email with anyone else.
                 </Form.Text>
               </Form.Group>

               <Form.Group controlId = "password">
                  <Form.Label>Password:</Form.Label>
                  <Form.Control
                      type = 'password'
                      placeholder = 'Please input your password here'
                      value = {password}
                      onChange = {e => setPassword(e.target.value)}
                      required
                    />
                </Form.Group>

                
                { isActive ?
                	<Button variant = 'primary' type = 'submit' id = 'submitBtn'> Login</Button>

                	:
                	<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>Login
                	</Button>
                }

           </Form>

		)

}