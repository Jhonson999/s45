import{useState, useEffect, useContext} from 'react'
import{Form, Button} from 'react-bootstrap'
import{Redirect} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'



export default function Register(){
    //State hooks to store the values of the input fields
 const{user, setUser} = useContext(UserContext)
	const[password1, setPassword1] = useState('')
	const[password2, setPassword2] = useState('')
	const[email, setEmail] = useState('')
 const[firstname, setFirstName] = useState('')
 const[lastname, setLastName] = useState('')
 const[mobilenumber, setMobileNumber] = useState('')


	//State to determine whether registered button is enabled or not
	const[isActive, setisActive] = useState(false)

	console.log(email)
	console.log(password1)
	console.log(password2)
 console.log(firstname)
 console.log(lastname)
 console.log(mobilenumber)

  
  function registerUser(e){
     //Prevents page redirection via form submission
     e.preventDefault();

     fetch('http://localhost:4000/users/checkEmail',{
      method: 'POST',
      headers:{
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      
      })
     })
     .then(res => res.json())
     .then(data => {
       

        if(data === true){
          Swal.fire({
            title: 'Duplicate email found!',
            icon: 'error',
            text: 'Please provide a different email.'
          })
        } else {
         fetch('http://localhost:4000/users/register',{
          method: 'POST',
          headers:{
            'Content-Type': 'application/json'
           },
           body: JSON.stringify({
        firstName: firstname,
        lastName: lastname,
        email: email,
        mobileNo: mobilenumber,
        password: password1

      
      })
         })
         .then(res => res.json())

            Swal.fire({
            title: 'Registration successful',
            icon: 'success',
            text: 'Welcome to Zuitt!'
          }).then(redirect => {
           window.location = "/login"
          })
        }
     })
     
     //Clears the input fields
     setEmail('');
     setPassword1('');
     setPassword2('');
     setFirstName('');
     setLastName('');
     setMobileNumber('');


   
  }

	useEffect(() => {
         if((email !== '' && password1 !== '' && password2 !== '' && firstname !== '' && lastname !== '' && mobilenumber !== '' && mobilenumber.length === 11) 
         	&& (password1.length >= 8 && password1 === password2)){

         	setisActive(true)
         } else {

         	setisActive(false)
         }

	}, [email, password1, password2,firstname,lastname,mobilenumber])


	return(

        (user.id !== null)?
            <Redirect to="/courses"/>
            :
           <Form onSubmit={(e)=>registerUser(e)}>
               <Form.Group>
                 <Form.Label>First Name:
                 </Form.Label>
                 <Form.Control
                   type = 'string'
                   placeholder = 'Please enter your first name here'
                   value = {firstname}
                   onChange = {e => setFirstName(e.target.value)}
                   required
                 />
                
               </Form.Group>
               <Form.Group>
                 <Form.Label>Last Name:
                 </Form.Label>
                 <Form.Control
                   type = 'string'
                   placeholder = 'Please enter your last name here'
                   value = {lastname}
                   onChange = {e => setLastName(e.target.value)}
                   required
                 />
                
               </Form.Group>
               <Form.Group>
                 <Form.Label>Email Address:
                 </Form.Label>
                 <Form.Control
                   type = 'email'
                   placeholder = 'Please enter your valid email here'
                   value = {email}
                   onChange = {e => setEmail(e.target.value)}
                   required
                 />
                 <Form.Text className = "text-muted">
                 We'll never share your email with anyone else.
                 </Form.Text>
               </Form.Group>
               <Form.Group>
                 <Form.Label>Mobile Number:
                 </Form.Label>
                 <Form.Control
                   type = 'string'
                   placeholder = 'Please enter your mobile number here'
                   value = {mobilenumber}
                   onChange = {e => setMobileNumber(e.target.value)}
                   required
                 />
                <Form.Text className = "text-muted">
                 Please enter 11-digit mobile number.
                 </Form.Text>
               </Form.Group>

               <Form.Group controlId = "password1">
                  <Form.Label>Password:</Form.Label>
                  <Form.Control
                      type = 'password'
                      placeholder = 'Please input your password here'
                      value = {password1}
                      onChange = {e => setPassword1(e.target.value)}
                      required
                    />
                    <Form.Text className = "text-muted">
                 Password should be atleast 8 characters long.
                 </Form.Text>
                </Form.Group>

                <Form.Group controlId = "password2">
                    <Form.Label>Verify Password: </Form.Label>
                    <Form.Control
                      type = 'password'
                      placeholder = 'Please verify your password'
                       value = {password2}
                      onChange = {e => setPassword2(e.target.value)}
                      required
                    />
                </Form.Group>

                { isActive ?
                	<Button variant = 'primary' type = 'submit' id = 'submitBtn'> Register</Button>

                	:
                	<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>Register
                	</Button>
                }

           </Form>

		)
}

//Ternary opearotr with the Button Tag

// ? - if -- if >Button isActive, the color will be primary and is enabled>
// : - else -- else <Button will be color danger and will be disabled>

// useEffect - react hook
// Syntax: useEffect (() =>{
//      <conditions>
// }, [parameter])

// --once a change happens in the parameter,