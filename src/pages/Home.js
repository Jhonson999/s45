import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
// import Course from '../components/Course'




export default function Home (){

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/",
		label: "Enroll now!"
	}
	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights/>
			{/*// <Course/>*/}
		</Fragment>

		)
}